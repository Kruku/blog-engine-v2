package com.example.projekt.services;

import com.example.projekt.dto.CommentDto;
import com.example.projekt.model.Comment;
import com.example.projekt.model.Entry;
import com.example.projekt.repository.CommentRepository;
import com.example.projekt.services.exceptions.CommentNotFoundException;
import com.example.projekt.services.exceptions.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private EntryService entryService;


    public CommentDto createComment(CommentDto commentDto) throws EntryNotFoundException {
        Entry entryById = entryService.findEntryById(commentDto.getEntryId());
        Comment comment = new Comment().builder()
                .author(commentDto.getAuthor())
                .comment(commentDto.getComment())
                .entId(commentDto.getEntryId())
                .entry(entryById)
                .build();
        commentRepository.save(comment);
        return commentMapper.comentToDto(comment);
    }

    public List<CommentDto> getAllComments() {
        List<Comment> comments = commentRepository.findAll();
        List<CommentDto> commentDtos = comments.stream()
                .map(comment -> commentMapper.comentToDto(comment))
                .collect(Collectors.toList());
        return commentDtos;
    }

    public List<CommentDto> deleteCommentsByEntryIds(List<Integer> entryIds) {
        List<Comment> commentsByEntryId = findCommentsByEntryIds(entryIds);
        commentRepository.deleteAll(commentsByEntryId);
        return commentMapper.commentsToDto(commentsByEntryId);
    }

    public CommentDto deleteCommentById(Integer id) throws CommentNotFoundException {
        Comment commentById = findCommentById(id);
        commentById.setEntry(new Entry());
        commentRepository.delete(commentById);
        return commentMapper.comentToDto(commentById);
    }

    public List<CommentDto> deleteCommentsByEntryId(Integer entryId) {
        List<Comment> commentsByEntryId = commentRepository.findAll()
                .stream()
                .filter(c -> c.getEntId().equals(entryId))
                .collect(Collectors.toList());
        commentRepository.deleteAll(commentsByEntryId);
        return commentMapper.commentsToDto(commentsByEntryId);
    }

    public List<Comment> findCommentsByEntryIds(List<Integer> entryIds) {
        return commentRepository
                .findAll()
                .stream()
                .filter(c -> entryIds.contains(c.getEntId()))
                .collect(Collectors.toList());
    }

    public Comment findCommentById(Integer id) throws CommentNotFoundException {
        List<Comment> allComments = commentRepository.findAll();
        return allComments.stream()
                .filter(c -> c.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new CommentNotFoundException());
    }
}
