package com.example.projekt.services;

import com.example.projekt.dto.CreateUserDto;
import com.example.projekt.dto.UserDto;
import com.example.projekt.dto.UserLoginDto;
import com.example.projekt.model.User;
import com.example.projekt.repository.UserRepository;
import com.example.projekt.services.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EntryService entryService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<UserDto> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserDto> usersDtos = users.stream()
                .map(user -> userMapper.userToDto(user))
                .collect(Collectors.toList());
        return usersDtos;
    }

    public boolean loginValidator(UserLoginDto userLoginDto) throws UserNotFoundException {
        User userByLogin = findUserByLogin(userLoginDto.getLogin());
        if (!passwordEncoder.matches(userLoginDto.getPassword(), userByLogin.getPassword())) {
            return false;
        }
        return true;
    }

    public UserDto createUser(CreateUserDto createDto) {
        if (createDto.getRole() == null || !createDto.getRole().equalsIgnoreCase("ADMIN")) {
            createDto.setRole("USER");
        }
        User createdUser = User.builder()
                .login(createDto.getLogin())
                .password(passwordEncoder.encode(createDto.getPassword()))
                .name(createDto.getName())
                .role(createDto.getRole().toUpperCase())
                .build();
        userRepository.save(createdUser);
        return userMapper.userToDto(createdUser);
    }

    public User findUserByLogin(String login) throws UserNotFoundException {
        return userRepository
                .findAll()
                .stream()
                .filter(user -> user.getLogin().equals(login))
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException());
    }

    public UserDto findUserByLoginReturnsDto(String login) throws UserNotFoundException {
        return userMapper.userToDto(findUserByLogin(login));
    }

    public UserDto setUserRole(UserDto dto) throws UserNotFoundException {
        User userByLogin = findUserByLogin(dto.getLogin());
        userByLogin.setRole(dto.getRole().toUpperCase());
        userRepository.save(userByLogin);
        return userMapper.userToDto(userByLogin);
    }

    public UserDto deleteUser(String login) throws UserNotFoundException {
        User userByLogin = findUserByLogin(login);

        entryService.deleteEntriesByUserLogin(login);
        userRepository.delete(userByLogin);
        return userMapper.userToDto(userByLogin);
    }

    public boolean userRoleValidator(HttpSession httpSession) {
        Optional<Object> userLogin = Optional.ofNullable(httpSession.getAttribute("userLogin"));
        if (!userLogin.isPresent()) {
            return false;
        }
        return true;
    }

    public boolean adminRoleValidator(HttpSession httpSession) {
        if (!userRoleValidator(httpSession)) {
            return false;
        }
        Optional<Object> userRole = Optional.ofNullable(httpSession.getAttribute("userRole"));
        if (!userRole.get().equals(true)) {
            return false;
        }
        return true;
    }

    public boolean hasAdminRole(String login) {
        try {
            User userByLogin = findUserByLogin(login);
            if (!userByLogin.getRole().equals("ADMIN")) {
                return false;
            }
            return true;
        } catch (UserNotFoundException e) {
            return false;
        }
    }

    public ModelAndView redirectsForUserRoleValidation(HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userRoleValidator(httpSession)) {
            httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
            return new ModelAndView("login");
        }
        return new ModelAndView("");
    }

    public ModelAndView redirectsForAdminRoleValidation(HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userRoleValidator(httpSession)) {
            httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
            return new ModelAndView("login");
        }
        if (!adminRoleValidator(httpSession)) {
            httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
            return new ModelAndView("handlers").addObject("userWithoutAdminRole", true);
        }
        return new ModelAndView("");
    }


}
