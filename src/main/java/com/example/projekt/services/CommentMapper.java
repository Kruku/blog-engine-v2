package com.example.projekt.services;

import com.example.projekt.dto.CommentDto;
import com.example.projekt.model.Comment;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CommentMapper {

    public CommentDto comentToDto(Comment comment) {
        CommentDto commentDto = new CommentDto().builder()
                .comment(comment.getComment())
                .author(comment.getAuthor())
                .id(comment.getId())
                .entryId(comment.getEntId())
                .build();
        return commentDto;
    }

    public List<CommentDto> commentsToDto(List<Comment> comments) {
        return comments.stream()
                .map(c -> new CommentMapper().comentToDto(c))
                .collect(Collectors.toList());
    }
}
