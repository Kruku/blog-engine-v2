package com.example.projekt.services;

import com.example.projekt.dto.AverageEntryRatingDto;
import com.example.projekt.dto.EntryRatingDto;
import com.example.projekt.model.Entry;
import com.example.projekt.model.EntryRating;
import com.example.projekt.repository.EntryRepository;
import com.example.projekt.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RatingService {
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private EntryRatingMapper entryRatingMapper;
    @Autowired
    private EntryRepository entryRepository;

    public EntryRatingDto rateEntry(String entryId, String rating, String login) {
        EntryRating entryRating = new EntryRating().builder()
                .rating(Integer.parseInt(rating))
                .entryId(Integer.parseInt(entryId))
                .login(login)
                .build();
        ratingRepository.save(entryRating);

        return entryRatingMapper.entryRatingToDto(entryRating);
    }

    public List<EntryRatingDto> getAllRatingsByEntryId(Integer entryId) {
        return ratingRepository.findAll().stream()
                .filter(rating -> rating.getEntryId().equals(entryId))
                .map(rating -> entryRatingMapper.entryRatingToDto(rating))
                .collect(Collectors.toList());
    }

    public List<AverageEntryRatingDto> averageRating() {
        List<Entry> all = entryRepository.findAll();
        List<AverageEntryRatingDto> entriesWithAverageRatings = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("#.##");
        for (int i = 0; i < all.size(); i++) {
            List<EntryRatingDto> allRatingsByEntryId = getAllRatingsByEntryId(all.get(i).getId());
            int entryId = all.get(i).getId();
            double sum = 0;
            for (int j = 0; j < allRatingsByEntryId.size(); j++) {
                sum += allRatingsByEntryId.get(j).getRating();
                if (j == allRatingsByEntryId.size() - 1) {
                    double averageRating = sum / allRatingsByEntryId.size();
                    entriesWithAverageRatings.add(new AverageEntryRatingDto(entryId,
                            df.format(averageRating), allRatingsByEntryId.size()));
                }
            }
        }
        return entriesWithAverageRatings;
    }
}
