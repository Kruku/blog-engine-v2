package com.example.projekt.services;

import com.example.projekt.dto.EntryRatingDto;
import com.example.projekt.model.EntryRating;
import org.springframework.stereotype.Component;

@Component
public class EntryRatingMapper {
    public EntryRatingDto entryRatingToDto(EntryRating entryRating) {
        return new EntryRatingDto().builder()
                .entryId(entryRating.getEntryId())
                .login(entryRating.getLogin())
                .rating(entryRating.getRating())
                .build();
    }
}
