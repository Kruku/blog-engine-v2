package com.example.projekt.services;

import com.example.projekt.dto.EntryDto;
import com.example.projekt.model.Entry;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EntryMapper {
    public EntryDto toDto(Entry entry) {
        return EntryDto.builder()
                .author(entry.getAuthor())
                .entry(entry.getEntry())
                .id(entry.getId())
                .title(entry.getTitle())
                .build();
    }

    public List<EntryDto> entriesToDto(List<Entry> entries) {
        return entries.stream()
                .map(e -> new EntryMapper().toDto(e))
                .collect(Collectors.toList());
    }
}
