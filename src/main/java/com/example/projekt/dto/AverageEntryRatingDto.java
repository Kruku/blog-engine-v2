package com.example.projekt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AverageEntryRatingDto {
    private Integer entryId;
    private String averageRating;
    private int totalRatings;

}
