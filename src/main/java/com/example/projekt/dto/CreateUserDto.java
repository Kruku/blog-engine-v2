package com.example.projekt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateUserDto {
    @NotNull(message = "Login can not be null")
    @NotBlank(message = "Login can not be blank")
    @Size(min = 4, message = "Login should be at least 4 characters long")
    private String login;
    @NotBlank(message = "Password can not be blank")
    @Size(min = 5, message = "Password should be at least 5 characters long")
    private String password;
    @NotBlank(message = "Password can not be blank")
    @Size(min = 5, message = "Password should be at least 5 characters long")
    private String repeatedPassword;
    private String role;
    @NotEmpty(message = "Name must be filled")
    @NotBlank(message = "Name can not be blank")
    @NotNull
    private String name;

}
