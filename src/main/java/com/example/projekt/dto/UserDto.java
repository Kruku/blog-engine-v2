package com.example.projekt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {
    @NotNull(message = "Login can not be null")
    @NotBlank(message = "Login field have to be filled")
    private String login;
    private Integer id;
    private String name;
    @NotNull(message = "User role can not be null")
    @NotBlank(message = "Role field have to be filled")
    private String role;
}
