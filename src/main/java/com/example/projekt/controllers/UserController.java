package com.example.projekt.controllers;

import com.example.projekt.dto.CreateUserDto;
import com.example.projekt.dto.UserDto;
import com.example.projekt.dto.UserLoginDto;
import com.example.projekt.services.UserService;
import com.example.projekt.services.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ModelAndView registeredUsersList(HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        List<UserDto> allUsers = userService.getAllUsers();
        return new ModelAndView("users").addObject("users", allUsers);
    }

    @GetMapping("/registration")
    public ModelAndView createUser(Model model) {
        model.addAttribute("user", new CreateUserDto());
        return new ModelAndView("registration");
    }

    @PostMapping("/registration")
    public ModelAndView createUser(@Valid @ModelAttribute(name = "user") CreateUserDto user,
                                   BindingResult bindingResult, HttpServletResponse httpServletResponse) {
        ModelAndView mav = new ModelAndView("registration");
        if (bindingResult.hasErrors()) {
            httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return mav;
        }
        try {
            userService.findUserByLogin(user.getLogin());
            String userWithSameLoginExists = "Login already taken";
            httpServletResponse.setStatus(HttpStatus.CONFLICT.value());
            return mav.addObject("userWithSameLoginExists", userWithSameLoginExists);
        } catch (UserNotFoundException e) {
            if (!user.getPassword().equals(user.getRepeatedPassword())) {
                String wrongRepeatedPassword = "Passwords have to match";
                httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
                return mav.addObject("wrongRepeatedPassword", wrongRepeatedPassword);
            }
        }
        mav.setViewName("handlers");
        userService.createUser(user);
        httpServletResponse.setStatus(HttpStatus.CREATED.value());
        return mav.addObject("userCreated", true);
    }

    @GetMapping("/login")
    public ModelAndView login(Model model) {
        model.addAttribute("userData", new UserLoginDto());
        return new ModelAndView("login");
    }

    @PostMapping("/login")
    public ModelAndView loginHandler(@ModelAttribute UserLoginDto userLoginDto, HttpSession httpSession,
                                     HttpServletResponse httpServletResponse) {
        String badRequest;
        try {
            if (!userService.loginValidator(userLoginDto)) {
                badRequest = "Password do not match to provided login";
                httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
                return new ModelAndView("login").addObject("badRequest", badRequest);
            }
            httpServletResponse.setStatus(HttpStatus.OK.value());
            httpSession.setAttribute("userRole", userService.hasAdminRole(userLoginDto.getLogin()));
            httpSession.setAttribute("userLogin", userLoginDto.getLogin());
            return new ModelAndView("login_handler");
        } catch (UserNotFoundException e) {
            badRequest = "User with provided login do not exist";
            httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return new ModelAndView("login").addObject("badRequest", badRequest);
        }
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpSession httpSession) {
        httpSession.invalidate();
        return new ModelAndView("redirect:/index");
    }

    @GetMapping("/users/{login}")
    public ModelAndView setUserRole(@PathVariable String login, HttpSession httpSession,
                                    HttpServletResponse httpServletResponse) {
        if (!userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse).getViewName().isEmpty()) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        ModelAndView mav = new ModelAndView();
        try {
            UserDto userByLogin = userService.findUserByLoginReturnsDto(login);
            mav.setViewName("set_user_role");
            return mav.addObject("user", userByLogin);
        } catch (UserNotFoundException e) {
            mav.setViewName("handlers");
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return mav.addObject("userNotFound", true);
        }
    }

    @PutMapping(value = "/users/{login}")
    public ModelAndView setUserRole(@Valid @ModelAttribute(name = "user") UserDto dto, HttpSession httpSession,
                                    HttpServletResponse httpServletResponse, BindingResult bindingResult) {
        ModelAndView mav = new ModelAndView("handlers");
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        } else if (bindingResult.hasErrors() || (!dto.getRole().equalsIgnoreCase("USER")
                && !dto.getRole().equalsIgnoreCase("ADMIN"))) {
            httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return mav.addObject("badRequest", true);
        } else {
            try {
                userService.setUserRole(dto);
                httpServletResponse.setStatus(HttpStatus.OK.value());
                return mav.addObject("userUpdated", true);
            } catch (UserNotFoundException e) {
                httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
                return mav.addObject("userNotFound", true);
            }
        }
    }

    @DeleteMapping("/users/{login}")
    public ModelAndView deleteUser(@PathVariable String login, HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        try {
            userService.deleteUser(login);
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return new ModelAndView("handlers").addObject("userDeleted", true)
                    .addObject("login", login);
        } catch (UserNotFoundException e) {
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return new ModelAndView("handlers").addObject("userNotFound", true);
        }
    }
}
