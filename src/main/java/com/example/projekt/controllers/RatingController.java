package com.example.projekt.controllers;

import com.example.projekt.services.EntryService;
import com.example.projekt.services.RatingService;
import com.example.projekt.services.UserService;
import com.example.projekt.services.exceptions.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class RatingController {
    @Autowired
    private RatingService ratingService;
    @Autowired
    private EntryService entryService;
    @Autowired
    private UserService userService;

    @GetMapping("/entries/{entryId}/{rating}")
    public ModelAndView entryRating(@PathVariable String entryId, @PathVariable String rating,
                                    HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userService.userRoleValidator(httpSession)) {
            return userService.redirectsForUserRoleValidation(httpSession, httpServletResponse);
        }
        Integer ratingAsInteger = Integer.parseInt(rating);
        if (ratingAsInteger > 5 || ratingAsInteger < 0) {
            httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return new ModelAndView("handlers").addObject("ratingBadRequest", true);
        }
        try {
            entryService.findEntryById(Integer.parseInt(entryId));
            ratingService.rateEntry(entryId, rating, httpSession.getAttribute("userLogin").toString());
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return new ModelAndView("handlers").addObject("correctlyVoted", true);
        } catch (EntryNotFoundException e) {
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return new ModelAndView("handlers").addObject("entryNotFound", true);
        }
    }
}
