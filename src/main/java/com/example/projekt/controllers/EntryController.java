package com.example.projekt.controllers;

import com.example.projekt.dto.*;
import com.example.projekt.services.CommentService;
import com.example.projekt.services.EntryService;
import com.example.projekt.services.RatingService;
import com.example.projekt.services.UserService;
import com.example.projekt.services.exceptions.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class EntryController {
    @Autowired
    private EntryService entryService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private RatingService ratingService;

    @RequestMapping("")
    public ModelAndView projectInformation() {
        return new ModelAndView("about");
    }

    @GetMapping("/index")
    public ModelAndView getAllEntries(Model model) {
        ModelAndView modelAndView = new ModelAndView("index");
        List<EntryDto> entryDtos = entryService.getAllEntries();
        List<CommentDto> commentDtos = commentService.getAllComments();
        List<AverageEntryRatingDto> ratingDtos = ratingService.averageRating();
        modelAndView.addObject("entries", entryDtos);
        modelAndView.addObject("comments", commentDtos);
        modelAndView.addObject("ratings", ratingDtos);
        model.addAttribute("comment", new CommentDto());
        model.addAttribute("rate", new String());
        return modelAndView;
    }

    @GetMapping("/entry")
    public ModelAndView createEntry(Model model, HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        model.addAttribute("entry", new EntryDto());
        return new ModelAndView("create_entry");
    }

    @PostMapping("/entry")
    public ModelAndView createEntry(@Valid @ModelAttribute(name = "entry") EntryDto entry,
                                    BindingResult bindingResult, HttpSession httpSession,
                                    HttpServletResponse httpServletResponse) {
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        if (bindingResult.hasErrors()) {
            httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return new ModelAndView("create_entry");
        }
        entry.setAuthor(httpSession.getAttribute("userLogin").toString());
        entryService.createEntry(entry);
        httpServletResponse.setStatus(HttpStatus.CREATED.value());
        return new ModelAndView("handlers").addObject("entryCreated", true);
    }

    @GetMapping("/entries")
    public ModelAndView entriesAsList(HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        List<EntryDto> allEntries = entryService.getAllEntries();
        return new ModelAndView("edit_entry").addObject("entries", allEntries);
    }

    @GetMapping("/entry/{id}")
    public ModelAndView editEntryById(@PathVariable String id, HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        try {
            EntryDto entryById = entryService.findEntryByIdReturnsDto(Integer.parseInt(id));
            return new ModelAndView("update_entry").addObject("entry", entryById);
        } catch (EntryNotFoundException e) {
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return new ModelAndView("handlers").addObject("entryNotFound", true);
        }
    }

    @PutMapping("/entry/{id}")
    public ModelAndView editEntryById(@Valid @ModelAttribute(name = "entry") EntryDto entry, BindingResult bindingResult,
                                      HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        if (bindingResult.hasErrors()) {
            httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return new ModelAndView("update_entry");
        }
        try {
            entry.setAuthor(httpSession.getAttribute("userLogin").toString());
            entryService.editEntry(entry);
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return new ModelAndView("handlers").addObject("entryUpdated", true);
        } catch (EntryNotFoundException e) {
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return new ModelAndView("handlers").addObject("entryNotFound", true);
        }
    }

    @DeleteMapping("/entry/{id}")
    public ModelAndView deleteEntryById(@PathVariable String id, HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        try {
            entryService.deleteEntryById(Integer.parseInt(id));
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return new ModelAndView("handlers").addObject("entryDeleted", true);
        } catch (EntryNotFoundException e) {
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return new ModelAndView("handlers").addObject("entryNotFound", true);
        }
    }

}
