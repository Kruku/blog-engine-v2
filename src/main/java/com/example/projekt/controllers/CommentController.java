package com.example.projekt.controllers;

import com.example.projekt.dto.CommentDto;
import com.example.projekt.services.CommentService;
import com.example.projekt.services.UserService;
import com.example.projekt.services.exceptions.CommentNotFoundException;
import com.example.projekt.services.exceptions.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@RequestMapping("/comments")
@Controller
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private UserService userService;

    @PostMapping
    public ModelAndView createComment(@Valid @ModelAttribute(name = "comment") CommentDto commentDto,
                                      HttpSession httpSession, HttpServletResponse httpServletResponse,
                                      BindingResult bindingResult) {
        ModelAndView mav = new ModelAndView("handlers");
        if (!userService.userRoleValidator(httpSession)) {
            return userService.redirectsForUserRoleValidation(httpSession, httpServletResponse);
        }
        if (bindingResult.hasErrors() || commentDto.getComment() == null || commentDto.getComment().length() < 1) {
            httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            return mav.addObject("commentValidationError", true);
        }
        try {
            String userLogin = (String) httpSession.getAttribute("userLogin");
            commentDto.setAuthor(userLogin);
            commentService.createComment(commentDto);
            httpServletResponse.setStatus(HttpStatus.CREATED.value());
            return mav.addObject("commentCreated", true);
        } catch (EntryNotFoundException e) {
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return mav.addObject("entryDoesNotExist", true);
        }
    }

    @GetMapping
    public ModelAndView allCommentsList(HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        List<CommentDto> allComments = commentService.getAllComments();
        return new ModelAndView("comments").addObject("comments", allComments);
    }

    @DeleteMapping("/{id}")
    public ModelAndView deleteComment(@PathVariable String id, HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (!userService.adminRoleValidator(httpSession)) {
            return userService.redirectsForAdminRoleValidation(httpSession, httpServletResponse);
        }
        try {
            commentService.deleteCommentById(Integer.parseInt(id));
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return new ModelAndView("handlers").addObject("commentDeleted", true);
        } catch (CommentNotFoundException e) {
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return new ModelAndView("handlers").addObject("commentNotFound", true);
        }
    }
}
