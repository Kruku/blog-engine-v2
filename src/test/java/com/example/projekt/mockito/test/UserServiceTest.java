package com.example.projekt.mockito.test;

import com.example.projekt.dto.CreateUserDto;
import com.example.projekt.dto.UserDto;
import com.example.projekt.model.User;
import com.example.projekt.repository.UserRepository;
import com.example.projekt.services.UserMapper;
import com.example.projekt.services.UserService;
import com.example.projekt.services.exceptions.UserNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @InjectMocks
    private UserService userService;
    @InjectMocks
    private UserMapper userMapper;


    @Captor
    ArgumentCaptor<User> userArgumentCaptor;

    @Test
    public void getAllUsers() throws Exception{
        when(userRepository.findAll()).thenReturn(new ArrayList<>());
        List<UserDto> userDtos = userService.getAllUsers();
        assertThat(userDtos.size()).isEqualTo(0);
    }
    @Test
    public void getAllUsers_shouldReturnCorrectDtos() throws Exception {
        ArrayList<User> users = new ArrayList<>();
        User user = new User().builder()
                .login("jack123")
                .name("Jackie")
                .role("USER")
                .build();
        user.setId(2);
        users.add(user);
        User admin = new User().builder()
                .login("admin1")
                .name("admin")
                .role("ADMIN")
                .id(5)
                .build();
        users.add(admin);
        when(userRepository.findAll()).thenReturn(users);
        assertThat(users.size()).isEqualTo(2);
        UserDto userDto = userMapper.userToDto(users.get(0));
        assertThat(userDto.getId()).isEqualTo(2);
        assertThat(userDto.getLogin()).isEqualTo("jack123");
        assertThat(userDto.getName()).isEqualTo("Jackie");
        assertThat(userDto.getRole()).isEqualTo("USER");
        UserDto adminDto = userMapper.userToDto(users.get(1));
        assertThat(adminDto.getLogin()).isEqualTo("admin1");
        assertThat(adminDto.getName()).isEqualTo("admin");
        assertThat(adminDto.getRole()).isEqualTo("ADMIN");
        assertThat(adminDto.getId()).isEqualTo(5);
    }

//    @Test
//    public void createUser_shouldReturnCorrectUserDto() throws Exception {
////        User exampleUser = new User(1, "exampleUser", "admin312", "admin", "ADMIN", null);
//
//        when(userRepository.findById(5)).thenReturn(null);
//        when(userRepository.save(any(User.class))).thenReturn(new User());
//        User createdUser = new User();
//        Mockito.when(userRepository.save(any(User.class))).thenReturn(createdUser);
//        CreateUserDto exampleUser = new CreateUserDto().builder()
//                .login("exampleUser")
//                .name("exampleUserName")
//                .password("example")
//                .repeatedPassword("example")
//                .role(null)
//                .build();
//        userService.createUser(exampleUser);
//        verify(userRepository, atLeast(1)).save(userArgumentCaptor.capture());
//        User sendedUser = userArgumentCaptor.getValue();
//        assertThat(sendedUser.getLogin()).isEqualTo("exampleUser");
//
//    }
//

}
