Blog engine.

Application based on spring model - view - controller framework with H2 Database.
 
Frameworks, templates, libraries used in project:

-Maven,

-Hibernate,

-Spring Boot,

-Spring Data,

-Spring Security,

-Swagger,

-Thymeleaf,

-Loombok.

Application functionality focuses on possibility to create blog without using WordPress. First of all functionality is divided 
between users without account on website and two designed types of role:

-Administrator,
 
-User.

Users without account have possibility to read entries or comments placed on website. They can also create new account with authomatically granted User role.

Users with granted User role can:

-create comments,

-rate entries.

Users with administrator role can:

-create/delete/update entries,

-create/delete comments,

-grant authorities to other accounts,

-delete accounts.

-delete users accounts.

